<?php
/**
 * This file is part of OXID eShop Community Edition.
 *
 * OXID eShop Community Edition is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OXID eShop Community Edition is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OXID eShop Community Edition.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2014
 * @version   OXID eShop CE
 */

$sLangName  = "Deutsch";
// -------------------------------
// RESOURCE IDENTIFIER = STRING
// -------------------------------
$aLang = array(
	'charset'                       => 'ISO-8859-15',
	'BTN_MY_FAVORITES'              => 'Meine Favoriten',
	'BTN_MYFAVORITES_LISTS'         => 'Meine Favoriten',
	'MYFAVORITES_LISTS_DESC'        => 'Sehen sie sich Ihre Favoriten Liste an',
	'TO_THE_BASKET'                 => 'In den Warenkorb',
	'SHOW_OPTIONS'                  => 'Varianten anzeigen',
	'ADD_TO_FAVORITE'               => 'Zur Favoritenliste hinzuf&uuml;gen',
	'ADDED_TO_BASKET_CONFIRMATION'  => 'Artikel zum Warenkorb hinzugef&uuml;gt',
	'OPEN_ALL_VARIANTS'             => 'Alle Varianten &ouml;ffnen',
    'SHOW_ARTICLE_DETAILS'          => 'Zeige Artikel Details',
    'AlleArtikel'                   => 'Alle Artikel anzeigen',
    'TOTAL_WEIGHT'                  => 'Gesamt Gewicht',
	'ACCOUNT_LOGIN_REQUIRE'         =>  'Sie m&uuml;ssen eingeloggt sein, um den H&auml;ndlershop nutzen zu k&ouml;nnen.',
	'Modal_Title'                   =>  'Artikel Details',
	'THANK_YOU_MESSAGE'             =>  'Vielen Dank f&uuml;r Ihre Nachricht. Wir melden uns.',
	'AGB_LINK_TITLE'                =>  'AGB',
	'IMPRINT_LINK_TITLE'            =>  'Impressum',
	'BTN_MY_ORDERS'                 =>  'Meine Bestellungen',
	'STREET'                        =>  'Strasse',
	'STREETNO'                      =>  'Hausnummer',
	'CITY'                          =>  'Stadt',
	'DATEI'							=>	'Upload',
	'SUBMITFILE'					=>	'hochladen',
	'ALLTOBASKET'					=>	'Alle Artikel in den Warenkorb',
	'RESET_AMOUNT'					=>	'Auswahl zur&uuml;cksetzen',
	'CA'							=>	'ca',
/* QUICKORDER */
	'QUICKORDER_BTN_ORDERCHANGE'    =>  'Bestellung Ändern',
	'QUICKORDER_BTN_ORDERMANDANTORY'=>  'Verbindlich bestellen',
	'QUICKORDER_ARTNUM'             =>  'Artikelnummer',
	'QUICKORDER_ARTICLE_DESC'       =>  'Artikel, Beschreibung, o.&Auml;.',
	'BTN_QUICKORDER'                =>  'Schnellbestellung',
	'SUBMIT_QUICK_ORDER'            =>  'Bestellung absenden',
	'QUICKORDER_PLACEHOLDER_SEARCHFIELD'	=>	'Artikelnummer oder Beschreibung',
/* Ende QUICKORDER */
/* TOOLTIPS */
	'TIP_ALLTOBASKET'				=>	'F&uuml;gt alle zuvor ausgew&auml;hlten Artikel dem Warenkorb hinzu',
	'TIP_SHOW_OPTIONS'				=>	'Zeigt alle verf&uuml;gbaren Varianten des Artikels an',
	'TIP_TO_THE_BASKET'				=>	'Legt die aus diesem Artikel ausgew&auml;hlte Anzahl in den Warenkorb',
	'TIP_MY_ORDERS'					=>	'Schauen Sie sich Ihre bisher get&auml;tigten Bestellungen an und legen die Bestellung einfach erneut in den Warenkorb',
	'TIP_MY_FAVORITES'				=>	'Schauen Sie sich Ihre Favoriten Liste an',
	'TIP_QUICKORDER'				=>	'F&uuml;hren Sie White-Label Bestellungen f&uuml;r Ihre Kunden aus',
	'TIP_RESET_AMOUNT'				=>	'Setzen Sie Ihre Warenkorbauswahl zur&uuml;ck',
/* Ende TOOLTIPS */


	'PLEASE_CHANGE_PASSWORD'		=>	'Bitte &auml;ndern Sie zu Ihrer Sicherheit Ihr Passwort!',
	'LOGGEDOUT_HEADLINE'			=>	'Automatischer Logout',
    'LOGGEDOUT_TEXT'				=>	'Sie wurden aufgrund l&auml;ngerer Inaktivit&auml;t automatisch ausgeloggt. ',
    'LOGGEDOUT_BUTTON'				=>	'Neu einloggen',
	'MESSAGE_NOT_ON_STOCK'			=>	'',
	'LOW_STOCK'						=>	'',
	'READY_FOR_SHIPPING'			=>	'',
	'WK_TEXT'						=>	'Die Versandkosten werden nach dem Packen der Ware hinzugef&uuml;gt. Wir geben uns grosse M&uuml;he, Ihre Sendung so zu packen dass die Versandkosten so kosteng&uuml;nstig wie m&ouml;glich ausfallen und berechnen nur die tats&auml;chlich anfallenden Kosten.',
	'DELTIMEUNIT'					=>	'Tage',
    'DELIVERYTIME_DELIVERYTIME'     =>  'Produktionszeit',
);
/*
[{ oxmultilang ident="ADDED_TO_BASKET_CONFIRMATION" }]
*/
