[{oxscript include="js/widgets/oxinputvalidator.js" priority=10 }]
[{oxscript add="$('form.js-oxValidate').oxInputValidator();"}]
<form class="js-oxValidate" action="[{ $oViewConf->getSelfActionLink() }]" name="order" method="post" method="POST" enctype="multipart/form-data">
	[{assign var="aErrors" value=$oView->getFieldValidationErrors()}]
	<div class="addressCollumns clear">
		<div class="collumn">
				[{$oViewConf->getHiddenSid()}]
				[{$oViewConf->getNavFormParams()}]
				<input type="hidden" name="fnc" value="changeuser_testvalues">
				<input type="hidden" name="cl" value="account_user">
				<input type="hidden" name="CustomError" value='user'>
				<input type="hidden" name="blshowshipaddress" value="1">
				<input type="hidden" name="MAX_FILE_SIZE" value="30000" />

			<div class="row">
				<div class="container-fluid">
					<h4 class="blockHead">
						[{oxmultilang ident="BILLING_ADDRESS"}]
					</h4>
					<div class="col-sm-2">&nbsp;</div>
					<div class="col-sm-10">
						<button id="userChangeAddress" class="btn btn-sm btn-info submitButton largeButton"
						        style="[{if !empty($aErrors)}]display: none;[{/if}]margin-left:0 !important;" name="changeBillAddress"
						        type="submit"><i class="glyphicon glyphicon-pencil"></i>&nbsp;[{ oxmultilang ident="CHANGE" }]</button>
					</div>
				</div>
			</div>

			<div class="form clear" [{if empty($aErrors)}]style="display: none;"[{/if}] id="addressForm">
				[{include file="form/fieldset/user_email.tpl"}]
				[{include file="form/fieldset/user_billing.tpl" noFormSubmit=true}]
			</div>

			<div class="row">
				<div class="container-fluid">
					<div class="col-sm-2">&nbsp;</div>
					<div class="col-sm-10">
						<br>
						<div class="form" id="addressText">
							[{include file="widget/address/billing_address.tpl"}]
						</div>
					</div>
				</div>
			</div>
			[{oxscript add="$('#userChangeAddress').click( function() { $('#addressForm').show();$('#userChangeAddress').hide();$('#addressText').hide();return false;});"}]
		</div>

		<div class="collumn">
			<div class="row">
				<div class="container-fluid">
					<h4 id="addShippingAddress" class="blockHead">
						[{oxmultilang ident="SHIPPING_ADDRESSES"}]
					</h4>
					<div class="col-sm-2">&nbsp;</div>
					<div class="col-sm-10">
						<button id="userChangeShippingAddress" class="btn btn-large btn-info btn-sm submitButton "
						        name="changeShippingAddress" type="submit"
						        style="[{if !$oView->showShipAddress() or !$oxcmp_user->getSelectedAddress()}] display: none; [{/if}] margin-left: 0 !important;"><i class="glyphicon glyphicon-pencil"></i>&nbsp;[{ oxmultilang ident="CHANGE" }]</button>
						<input type="checkbox" name="blshowshipaddress" id="showShipAddress" [{if !$oView->showShipAddress()}]checked[{/if}] value="0">
						<label for="showShipAddress">[{oxmultilang ident="USE_BILLINGADDRESS_FOR_SHIPPINGADDRESS" }]</label>
					</div>
				</div>
			</div>

			<div id="shippingAddress" class="form-horizontal" [{if !$oView->showShipAddress()}] style="display: none;" [{/if}]>
				[{include file="form/fieldset/user_shipping.tpl" noFormSubmit=true}]
				[{*include file="form/fieldset/user_shipping.tpl"*}]
			</div>

			[{oxscript add="$('#showShipAddress').change( function() { $('#userChangeShippingAddress').toggle($(this).is(':not(:checked)') &&  $('#addressId').val() != -1 ); $('#shippingAddress').toggle($(this).is(':not(:checked)')); });"}]
			[{oxscript add="$('#addressId').change(function() { $('#userChangeShippingAddress').toggle($('#addressId').val() != -1 ); }); "}]

		</div>
	</div>
</form>

