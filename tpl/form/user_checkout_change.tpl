[{oxscript include="js/widgets/oxinputvalidator.js" priority=10 }]
[{oxscript add="$('form.js-oxValidate').oxInputValidator();"}]
[{block name="user_checkout_change"}]
    <form role="form" class="form-horizontal js-oxValidate" action="[{$oViewConf->getSslSelfLink()}]" name="order" method="post" enctype="multipart/form-data">
    [{block name="user_checkout_change_form"}]
        [{assign var="aErrors" value=$oView->getFieldValidationErrors()}]
        [{ $oViewConf->getHiddenSid() }]
        [{ $oViewConf->getNavFormParams() }]
        <input type="hidden" name="cl" value="user">
        <input type="hidden" name="option" value="[{$oView->getLoginOption()}]">
        <input type="hidden" name="fnc" value="changeuser">
        <input type="hidden" name="lgn_cook" value="0">
        <input type="hidden" name="blshowshipaddress" value="1">
        <div class="lineBox clear">
            <a href="[{oxgetseourl ident=$oViewConf->getBasketLink() }]" class="prevStep submitButton largeButton btn btn-warning" id="userBackStepTop"><i class="glyphicon glyphicon-arrow-left"></i>&nbsp;[{oxmultilang ident="PREVIOUS_STEP"}]</a>
            <button id="userNextStepTop" class="submitButton largeButton nextStep btn btn-success pull-right" name="userform" type="submit">[{oxmultilang ident="CONTINUE_TO_NEXT_STEP"}]&nbsp;<i class="glyphicon glyphicon-arrow-right"></i></button>
        </div>
        <div class="checkoutCollumns clear">
            <!--<div class="collumn">-->
                [{block name="user_checkout_billing"}]

                    [{* Rechnungsadresse Head *}]
                    [{block name="user_checkout_billing_head"}]
                        <div class="form-group">
                            <label class="col-sm-2 control-label norm" for="userChangeAddress"><strong>[{oxmultilang ident="BILLING_ADDRESS" suffix="COLON"}]</strong></label>
                            <div class="col-sm-10">
                                <button id="userChangeAddress" style="margin-left: 0 !important;" class="submitButton largeButton btn btn-info" name="changeBillAddress" type="submit">[{oxmultilang ident="CHANGE" }]&nbsp;<i class="glyphicon glyphicon-pencil"></i></button>
                            </div>
                        </div>
                        [{*}]<h4 class="blockHead">
                            [{oxmultilang ident="BILLING_ADDRESS" }]
                            <button id="userChangeAddress" class="submitButton largeButton btn btn-info" name="changeBillAddress" type="submit">[{oxmultilang ident="CHANGE" }]&nbsp;<i class="glyphicon glyphicon-pencil"></i></button>
                        </h4>[{*}]
                        [{oxscript add="$('#userChangeAddress').click( function() { $('#addressForm').show();$('#changeinfo').show();$('#addressText').hide();$('#userChangeAddress').hide();return false;});"}]
                        [{if $aErrors}]
                            [{oxscript add="$(document).ready(function(){ $('#userChangeAddress').trigger('click');});"}]
                        [{/if}]
                    [{/block}]

                    [{* Rechnungsadresse Formular *}]
                    [{block name="user_checkout_billing_form"}]
                        <div id="changeinfo" class="alert alert-danger" style="display: none">[{oxmultilang ident="CHANGE_RECHNUNG_ERROR"}] <a href="mailto:sales@bodynova.com">email</a></div>
                        <div class="form-group" style="display: none;" id="addressForm">
                            [{include file="form/fieldset/user_billing.tpl" noFormSubmit=true blSubscribeNews=true blOrderRemark=true}]
                        </div>
                    [{/block}]

                    [{* Rechnungsadresse Anzeige *}]
                    [{block name="user_checkout_billing_feedback"}]
                        <div class="form-group">
                            <label class="col-sm-2 control-label norm" for="showShipAddress" >[{* oxmultilang ident="RECHNUNGSADRESSE" suffix="COLON"*}]</label>
                            <div class="col-sm-10" style="margin-top: 8px;">
                                [{include file="widget/address/billing_address.tpl" noFormSubmit=true blSubscribeNews=true blOrderRemark=true}]
                            </div>
                        </div>
                    [{/block}]
                [{/block}]
            <!--</div>-->

                [{block name="user_checkout_shipping"}]

                    [{* Lieferadresse Head *}]
                    [{block name="user_checkout_shipping_head"}]
	                    <div class="form-group">
		                    <label class="col-sm-2 control-label norm" for="showShipAddress"><strong>[{oxmultilang ident="SHIPPING_ADDRESS" suffix="COLON"}]</strong></label>
							<div class="col-sm-10">
								<button id="userChangeShippingAddress" onclick="myscrollTo(840)" style="margin-left: 0 !important;" class="submitButton largeButton btn btn-info" name="changeShippingAddress" type="submit" [{if !$oView->showShipAddress() or !$oxcmp_user->getSelectedAddress()}] style="display: none;" [{/if}]>
									[{oxmultilang ident="CHANGE"}]&nbsp;<i class="glyphicon glyphicon-pencil"></i>
								</button>

							</div>

		                </div>
                        [{*}]<h4 class="blockHead">
                            [{oxmultilang ident="SHIPPING_ADDRESS"}]
                            <button id="userChangeShippingAddress" class="submitButton largeButton btn btn-info" name="changeShippingAddress" type="submit" [{if !$oView->showShipAddress() or !$oxcmp_user->getSelectedAddress()}] style="display: none;" [{/if}]>
                                [{oxmultilang ident="CHANGE"}]&nbsp;<i class="glyphicon glyphicon-pencil"></i>
                            </button>
                        </h4>[{*}]
                        [{oxscript add="$('#showShipAddress').change(function() { $('#userChangeShippingAddress').toggle($(this).is(':not(:checked)') && $('#addressId').val() != -1 ); }); "}]
                        [{oxscript add="$('#addressId').change(function() { $('#userChangeShippingAddress').toggle($('#addressId').val() != -1 );}); "}]
                    [{/block}]

                    [{* Rechnungsadresse als Lieferadresse verwenden Checkbox *}]
                    [{block name="user_checkout_shipping_change"}]
                        <div class="form-group">
                            <label class="col-sm-2 control-label norm" for="showShipAddress">[{ oxmultilang ident="RECHNUNGSADRESSE" suffix="COLON"}]</label>
                            <div class="col-sm-10">
                                <input style="margin-top: 10px;margin-left:10px;" class="" type="checkbox" name="blshowshipaddress" id="showShipAddress" [{if !$oView->showShipAddress()}]checked[{/if}] value="0">
                                <span class="help-block">[{oxmultilang ident="USE_BILLINGADDRESS_FOR_SHIPPINGADDRESS"}]</span>
                            </div>
                        </div>
                        [{oxscript add="$('#showShipAddress').change( function(e){ changeShipAdress(e)} );"}]
                    [{/block}]

                    [{* Lieferadresse Formular *}]
                    [{block name="user_checkout_shipping_form"}]
                        <div id="shippingAddress" class="form-group" [{if !$oView->showShipAddress()}]style="display: none;"[{/if}]>
                            [{include file="form/fieldset/user_shipping.tpl" noFormSubmit=true onChangeClass='user'}]
                        </div>
                    [{/block}]

                    [{* Newsletter und Mitteilung *}]
                    [{block name="user_checkout_shipping_feedback"}]
                        [{include file="form/fieldset/order_newsletter.tpl" blSubscribeNews=true}]
                        [{include file="form/fieldset/order_remark.tpl" blOrderRemark=true}]
                    [{/block}]

                [{/block}]

        </div>
        <div class="lineBox clear">
            <a href="[{oxgetseourl ident=$oViewConf->getBasketLink()}]" class="prevStep submitButton largeButton btn btn-warning pull-left" id="userBackStepBottom"><i class="glyphicon glyphicon-arrow-left"></i>&nbsp;[{oxmultilang ident="PREVIOUS_STEP"}]</a>
            <button id="userNextStepBottom" class="submitButton largeButton nextStep btn btn-success pull-right" name="userform" type="submit">[{oxmultilang ident="CONTINUE_TO_NEXT_STEP"}]&nbsp;<i class="glyphicon glyphicon-arrow-right"></i></button>
        </div>
    [{/block}]
    </form>
[{/block}]
