[{oxscript include="js/widgets/oxinputvalidator.js" priority=10 }]
[{oxscript add="$('form.js-oxValidate').oxInputValidator();"}]
<form class="js-oxValidate" action="[{$oViewConf->getSelfActionLink()}]" name="changepassword" method="post">
    [{assign var="aErrors" value=$oView->getFieldValidationErrors()}]
        [{ $oViewConf->getHiddenSid() }]
        [{ $oViewConf->getNavFormParams() }]
        <input type="hidden" name="fnc" value="changePassword">
        <input type="hidden" name="cl" value="account_password">
        <input type="hidden" name="CustomError" value='user'>
        <input type="hidden" id="passwordLength" value="[{$oViewConf->getPasswordLength()}]">
    <div class="form clear form-group[{if $oView->isPasswordChanged() }] hide[{/if}]">

        [{assign var="Passwort" value=$oxcmp_user->oxuser__oxpassword}]
        [{assign var="Passsalt" value=$oxcmp_user->oxuser__oxpasssalt}]
        [{assign var="standartpasswort" value="76ed099a31812ef8cec4acdc6ec71575"}]
        [{assign var="standartpasssalt" value="3235613333363166636533383266383031383565633830393038633265616466"}]

        [{if $Passwort != $standartpasswort && $Passsalt != $standartpasssalt }]
            <div class="form-group[{if $aErrors.oxuser__oxpassword}] oxInValid[{/if}]">
                <label for="passwordOld">[{ oxmultilang ident="OLD_PASSWORD" suffix="COLON" }]</label>
                <input type="password" id="passwordOld" name="password_old" class="js-oxValidate js-oxValidate_notEmpty textbox form-control" placeholder="Altes Passwort">
                <p class="oxValidateError">
                    <span class="js-oxError_notEmpty">[{ oxmultilang ident="ERROR_MESSAGE_INPUT_NOTALLFIELDS" }]</span>
                    [{include file="message/inputvalidation.tpl" aErrors=$aErrors.oxuser__oxpassword}]
                </p>
            </div>
        [{/if}]
        [{if $Passwort == $standartpasswort && $Passsalt == $standartpasssalt}]
            <input type="hidden" id="passwordOld" name="password_old" value="bodynova">
        [{/if}]

        <div class="form-group[{if $aErrors.oxuser__oxpassword}] oxInValid[{/if}]">
            <label for="passwordNew">[{ oxmultilang ident="NEW_PASSWORD" suffix="COLON" }]</label>
            <input type="password" id="passwordNew" name="password_new" class="js-oxValidate js-oxValidate_notEmpty js-oxValidate_length js-oxValidate_match textbox form-control" placeholder="Neues Passwort">
            <p class="oxValidateError">
                <span class="js-oxError_notEmpty">[{ oxmultilang ident="ERROR_MESSAGE_INPUT_NOTALLFIELDS" }]</span>
                <span class="js-oxError_length">[{ oxmultilang ident="ERROR_MESSAGE_PASSWORD_TOO_SHORT" }]</span>
                <span class="js-oxError_match">[{ oxmultilang ident="ERROR_MESSAGE_USER_PWDDONTMATCH" }]</span>
                [{include file="message/inputvalidation.tpl" aErrors=$aErrors.oxuser__oxpassword}]
            </p>
        </div>
        <div class="form-group[{if $aErrors.oxuser__oxpassword}] oxInValid[{/if}]">

            <label for="passwordNewConfirm">[{ oxmultilang ident="CONFIRM_PASSWORD" suffix="COLON" }]</label>
            <input type="password" id="passwordNewConfirm" name="password_new_confirm" class="js-oxValidate js-oxValidate_notEmpty js-oxValidate_length js-oxValidate_match textbox form-control" placeholder="Passwort bestätigen">
            <p class="oxValidateError">
                <span class="js-oxError_notEmpty">[{ oxmultilang ident="ERROR_MESSAGE_INPUT_NOTALLFIELDS" }]</span>
                <span class="js-oxError_length">[{ oxmultilang ident="ERROR_MESSAGE_PASSWORD_TOO_SHORT" }]</span>
                <span class="js-oxError_match">[{ oxmultilang ident="ERROR_MESSAGE_USER_PWDDONTMATCH" }]</span>
                [{include file="message/inputvalidation.tpl" aErrors=$aErrors.oxuser__oxpassword}]
            </p>

        </div>

            <button id="savePass" type="submit" class="submitButton btn btn-info">[{ oxmultilang ident="SAVE" }]</button>

    </div>
</form>
