[{capture append="oxidBlock_pageBody"}]

	[{if !$oxcmp_user->oxuser__oxpassword->value}]
		[{if $oView->getClassName() == "forgotpwd"}]

			<div class="container">
				<a href="[{$oViewConf->getHomeLink()}]" title="[{$oxcmp_shop->oxshops__oxtitleprefix->value}]">
					<img alt="Bodynova Logo" src="[{$oViewConf->getImageUrl('logo-small.png')}]">
				</a>

				[{if $oView->showUpdateScreen() }]
					[{assign var="template_title" value="NEW_PASSWORD"|oxmultilangassign}]
				[{elseif $oView->updateSuccess() }]
					[{assign var="template_title" value="CHANGE_PASSWORD"|oxmultilangassign}]
				[{elseif !$oView->updateSuccess()}]
					[{assign var="template_title" value="FORGOT_PASSWORD"|oxmultilangassign}]
				[{/if}]

				<h1 class="pageHead">[{$template_title}]</h1>

				[{if $oView->isExpiredLink() }]
				<div class="box info">[{oxmultilang ident="ERROR_MESSAGE_PASSWORD_LINK_EXPIRED"}]</div>
				[{elseif $oView->showUpdateScreen() }]
				[{include file="form/forgotpwd_change_pwd.tpl"}]
				[{elseif $oView->updateSuccess() }]

				<div class="box info">[{oxmultilang ident="PASSWORD_CHANGED"}]</div>

				<div class="bar">
					<form action="[{$oViewConf->getSelfActionLink()}]" name="forgotpwd" method="post">
						<div>
							[{ $oViewConf->getHiddenSid() }]
							<input type="hidden" name="cl" value="start">
							<button id="backToShop" class="submitButton largeButton" type="submit">
								[{oxmultilang ident="BACK_TO_SHOP"}]
							</button>
						</div>
					</form>
				</div>
				[{else}]
				[{if $oView->getForgotEmail()}]
				<div class="box info">[{oxmultilang ident="PASSWORD_WAS_SEND_TO" suffix="COLON"}] [{$oView->getForgotEmail()}]</div>
				<div class="bar">
					<form action="[{$oViewConf->getSelfActionLink()}]" name="forgotpwd" method="post">
						<div>
							[{$oViewConf->getHiddenSid()}]
							<input type="hidden" name="cl" value="start">
							<button id="backToShop" class="submitButton largeButton" type="submit">
								[{oxmultilang ident="BACK_TO_SHOP"}]
							</button>
						</div>
					</form>
				</div>
				[{else}]
				[{include file="form/forgotpwd_email.tpl"}]
				[{/if}]
				[{/if}]

				[{if !$oView->isActive('PsLogin') }]
				[{insert name="oxid_tracker" title=$template_title }]
				[{/if}]
			</div>
		[{else}]
			[{include file="widget/header/cookienote.tpl"}]
			<div class="container">
				<a href="[{$oViewConf->getHomeLink()}]" title="[{$oxcmp_shop->oxshops__oxtitleprefix->value}]">
					<img alt="Bodynova Logo" src="[{$oViewConf->getImageUrl('logo-small.png')}]">
				</a>
				<div class="accountLoginView">
					<h1 id="loginAccount" class="pageHead">[{$oView->getTitle()}]</h1>
					<p>[{ oxmultilang ident="LOGIN_ALREADY_CUSTOMER"}]</p>
					[{include file="form/login_account.tpl"}]
				</div>
			</div>
		[{/if}]

	[{else}]
		[{if $oView->showRDFa()}]
			[{include file="rdfa/rdfa.tpl"}]
		[{/if}]
		[{include file="layout/header.tpl"}]
		[{*include file="widget/newsModal.tpl"*}]
		<div id="wrapper" class="container-fluid">
			<!--<div id="content" class="container">-->
			<div class="row">
				[{if $sidebarLeft == 1}]
					<!-- <ul class="nav nav-pills nav-stacked" data-spy="affix" data-offset-top="205">-->
					<div id="sidebarLeft" class="col-xs-3 col-sm-3 col-md-2 col-lg-2 affix-top" data-spy="affix"
					     data-offset-top="205">
						[{*include file="layout/sidebar.tpl"*}]
						[{include file="layout/sidebarLeft.tpl"}]
					</div>
				[{/if}]
				<!--<div id="contentwrapper">-->
				<div id="middelContent"
				     class="col-xs-8 col-sm-8 col-md-7 col-lg-7 col-xs-push-3 col-sm-push-3 col-md-push-2 col-lg-push-2"
				     role="main">
					[{* Banner-Slider *}]
					<!--<div class="slidercontent">-->
					[{* Banner *}]
					[{if $oView->getClassName()=='start' && $oView->getBanners()|@count > 0 }]
						<!--<div class="oxSlider">-->
						[{*include file="widget/promoslider.tpl" *}]
						[{include file="widget/promosliderNeu.tpl" }]
						<!--</div>-->
					[{/if}]
					<!--</div>-->
					[{if $oView->getClassName() ne "start" &&
						!$blHideBreadcrumb &&
						$oView->getClassName() ne "alist" &&
						$oView->getClassName() ne "search" &&
						$oView->getClassName() ne "basket" &&
						$oView->getClassName() ne "user" &&
						$oView->getClassName() ne "order" &&
						$oView->getClassName() ne "thankyou" &&
						$oView->getClassName() ne "quickorder" &&
						$oView->getClassName() ne "contact" &&
						$oView->getClassName() ne "account" &&
						$oView->getClassName() ne "myfavorites" &&
						$oView->getClassName() ne "content" &&
						$oView->getClassName() ne "account_newsletter" &&
						$oView->getClassName() ne "account_password" &&
						$oView->getClassName() ne "account_user" &&
						$oView->getClassName() ne "account_order" &&
						$oView->getClassName() ne "adressverwaltung"}]
						[{include file="widget/breadcrumb.tpl"}]
					[{/if}]
					[{include file="message/errors.tpl"}]
					[{foreach from=$oxidBlock_content item="_block"}]
						[{$_block}]
						<br>
					[{/foreach}]
				</div>
				<!--</div>-->
				[{if $sidebarRight == 1}]
					<div id="sidebarRight" class="hidden-xs hidden-sm col-md-3 col-lg-3 affix" role="complementary">
						<div id="fixedWarenkorb" class="affix-top">
							[{include file="layout/sidebarRight.tpl"}]
						</div>
					</div>
				[{/if}]
			</div>
			<!--</div>-->
			[{*include file="layout/footer.tpl"*}]
			[{*<div id="page" class="container [{if $sidebar}] sidebar[{$sidebar}][{/if}]"></div>*}]
			[{*include file="widget/facebook/init.tpl"*}]
			[{*if $oView->isPriceCalculated() *}]
			[{block name="layout_page_vatinclude"}]
				[{oxifcontent ident="oxdeliveryinfo" object="oCont"}]
				[{assign var="tsBadge" value=""}]
					[{* if $oView->getTrustedShopId()}]
						[{assign var="tsBadge" value="TsBadge"}]
					[{/if *}]
					<div id="incVatMessage[{$tsBadge}]">
						* <span class="deliveryInfo">[{ oxmultilang ident="PLUS" }]<a href="" rel="nofollow">[{oxmultilang ident="PLUS_SHIPPING2"}]</a></span>
						[{if $oView->isVatIncluded()}]
							*
							<span class="deliveryInfo">[{ oxmultilang ident="PLUS_SHIPPING" }]<a
										href="[{$oCont->getLink() }]"
										rel="nofollow">[{oxmultilang ident="PLUS_SHIPPING2"}]</a></span>
						[{else}]
							*
							<span class="deliveryInfo">[{ oxmultilang ident="PLUS" }]<a href="[{$oCont->getLink()}]" rel="nofollow">[{oxmultilang ident="PLUS_SHIPPING2"}]</a></span>
						[{/if}]
					</div>
				[{/oxifcontent}]
				<div id="incVatMessage">
					<a href="#" class="back-to-top"><i class="fa fa-arrow-circle-up"></i></a>
					<span class="deliveryInfo">[{oxmultilang ident="INCL_TAX_AND_PLUS_SHIPPING"}]</span>
				</div>
			[{/block}]
			[{*/if*}]
		</div>
	[{/if}]
[{/capture}]
[{include file="layout/base.tpl"}]
