[{block name="sidebarRight"}]
	[{block name="Warenkorb"}]
		<div class="panel panel-default">
			<div class="panel-heading">
				<h3 class="panel-title">
					[{oxmultilang ident="PAGE_TITLE_BASKET" }]
				</h3>
			</div>
				[{if $oView->isLanguageLoaded()}]
					[{if $oxcmp_user->oxuser__wkansicht->rawValue == 1}]
						<div class="panel-body">
							<form class="navbar-form" role="form">
								<table class="table table-striped [{*}]table-bordered[{*}]">
									[{*}]<thead>
										<tr>
											<td></td>
											<td></td>
											<td></td>
										</tr>
									</thead>[{*}]
									<tbody>
										[{* Warenkorb Inhalt: *}]
										[{assign var="nummer" value=0}]
										[{foreach from=$oxcmp_basket->getContents() name=miniBasketList item=_product}]
											[{block name="widget_minibasket_product"}]
												[{math assign="nummer" equation="x + y" x=$nummer y=1 }]
												[{assign var="minibasketItemTitle" value=$_product->getTitle()}]
												[{assign var="basketitemkey" value=$_product->getBasketItemKey()}]
												[{assign var="artnum" value=$_product->getArtNum()}]
												[{* $_product->setSort($nummer) *}]
												[{* assign var="sort" value=$_product->getSort() *}]
												[{* $_product->getParentId()|var_dump *}]
												<tr onclick="jumpToArticle('[{$_product->getParentId()}]','[{$_product->getProductId()}]');" style="cursor: pointer">
													<td class="nonePadding">
														<span class="badge sidebarBadge" style="z-index: 10000">[{$_product->getAmount()}]</span>
														<div class="pictureBoxSB-NoBig hidden-md"><img  src="https://bodynova.de/out/imagehandler.php?artnum=[{$artnum}]&size=450_450_100" class="img-thumbnail"></div>
													</td>
													<td>
														[{$minibasketItemTitle|strip_tags}]
													</td>
													<td class="align-right">
														<strong>[{oxprice price=$_product->getPrice() currency=$currency}]</strong>
													</td>
												</tr>
											[{/block}]
										[{/foreach}]
										[{* Leere Zeile: *}]
										<tr>
											<td>&nbsp;</td>
											<td>&nbsp;</td>
											<td>&nbsp;</td>
										</tr>
										[{* Rabatte: *}]
										[{foreach from=$oxcmp_basket->getDiscounts() item=oDiscount name=test_Discounts}]
											[{*$oDiscount->formatDiscount()*}]
											<tr>
												<td colspan="2"><strong>[{if $oDiscount->dDiscount < 0}][{oxmultilang ident="SURCHARGE"}] [{else}][{oxmultilang ident="DISCOUNT"}] [{/if}]&nbsp;[{$oDiscount->sDiscount}][{*$oDiscount|var_dump*}]</strong></td>
												<td class="totalCol align-right"><strong>[{oxprice price=$oDiscount->dDiscount*-1 currency=$currency}]</strong></td>
											</tr>
										[{/foreach}]
										[{* GesamtSumme: *}]
										[{block name="widget_minibasket_total"}]
											<tr>
												<td colspan="2"><strong>[{oxmultilang ident="TOTAL"}]</strong></td>
												<td class="align-right" style="padding-left:0;"><strong>
														[{if $oxcmp_basket->isPriceViewModeNetto()}]
															[{oxprice price=$oxcmp_basket->_getDiscountedProductsSum() currency=$currency}]
														[{else}]
															[{$oxcmp_basket->getFProductsPrice()}]
														[{/if}]
													</strong>
												</td>
											</tr>
										[{/block}]
									</tbody>
								</table>
							</form>
						</div>
					[{/if}]
				[{/if}]
				<div class="panel-footer">
					[{* Wk-Ansicht Buttons: *}]
					[{if $oxcmp_user->oxuser__wkansicht->rawValue == 1}]
						<button style="margin-bottom:0; margin-left: 0 !important;" type="button" class="btn btn-info btn btn-default ladda-button bntooltip" data-style="expand-right" onclick="setwkansicht('[{$oxcmp_user->oxuser__oxid->value}]',0)" data-toggle="tooltip" data-placement="right" title="[{oxmultilang ident="TT-WK-CLOSE"}]" ><span class="glyphicon glyphicon-chevron-up"></span></button>
						<a style="margin-bottom:0;margin-right:0;float:right;" class="minib btn btn-success btn btn-default ladda-button bntooltip" href="[{oxgetseourl ident=$oViewConf->getSelfLink()|cat:"cl=basket" }]" data-toogle="tooltip" data-placement="left" title="[{oxmultilang ident="TT-TO-CHECKOUT"}]"><span class="glyphicon glyphicon-shopping-cart"></span> [{oxmultilang ident="CHECKOUT"}]</a>
					[{else}]
						<button style="margin-bottom:10px" type="button" class="btn btn-info btn btn-default ladda-button bntooltip" data-style="expand-right" onclick="setwkansicht('[{$oxcmp_user->oxuser__oxid->value}]' , 1)" data-toggle="tooltip" data-placement="right" title="[{oxmultilang ident="TT-WK-OPEN"}]" ><span class="glyphicon glyphicon-chevron-down"></span></button>
					[{/if}]
					[{oxscript widget=$oView->getClassName()}]
				</div>
		</div>
	[{/block}]
[{* DEBUG Panel only Visible for Admins: *}]
	[{*if $oxcmp_user->oxuser__oxrights->value eq 'malladmin'}]
		<div class="panel panel-info">
			<div class="panel-heading">
				<h3 class="panel-title">DEBUG: Only for Admins</h3>
			</div>
			<div class="panel-body">
				Oxcmp_Basket:<br>
				<pre>
					[{$oxcmp_basket|var_dump}]
				</pre>
			</div>
		</div>
	[{/if*}]
[{/block}]
