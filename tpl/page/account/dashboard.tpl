[{assign var="template_title" value="MY_ACCOUNT"|oxmultilangassign }]
[{assign var="dropship" value=$oxcmp_user->oxuser__dropship->value}]
[{capture append="oxidBlock_content"}]
	<div class="panel panel-default">
		<div class="panel-heading">
			<h1 class="panel-title">[{ oxmultilang ident="MY_ACCOUNT" }] - "[{ $oxcmp_user->oxuser__oxusername->value }]
				"</h1>
			<div class="pull-right" style="margin-top: -27px;">
				[{if $oView->getClassName() eq "account" && !$blHideBreadcrumb}]
					[{include file="widget/breadcrumb.tpl"}]
				[{/if}]
			</div>
		</div>
		<div class="panel-body">
			[{block name="account_dashboard_col1"}]
				<ul class="list-group" style="margin-bottom: 2px;">
					<li class="list-group-item">
						<a id="linkAccountPassword"
						   href="[{ oxgetseourl ident=$oViewConf->getSslSelfLink()|cat:"cl=account_password" }]"
						   rel="nofollow">
							[{ oxmultilang ident="CHANGE_PASSWORD" }]
						</a>
					</li>
					<li class="list-group-item">
						<a id="linkAccountNewsletter"
						   href="[{ oxgetseourl ident=$oViewConf->getSslSelfLink()|cat:"cl=account_newsletter" }]"
						   rel="nofollow">
							[{ oxmultilang ident="NEWSLETTER_SETTINGS" }]
						</a><br>
						[{ oxmultilang ident="NEWSLETTER_SUBSCRIBE_CANCEL" }]
					</li>
					<li class="list-group-item">
						<a id="linkAccountBillship"
						   href="[{ oxgetseourl ident=$oViewConf->getSslSelfLink()|cat:"cl=account_user" }]" rel="nofollow">
							[{ oxmultilang ident="BILLING_SHIPPING_SETTINGS" }]
						</a><br>
						[{ oxmultilang ident="UPDATE_YOUR_BILLING_SHIPPING_SETTINGS" }]
					</li>
					<li class="list-group-item">
						<a id="linkAccountOrder"
						   href="[{ oxgetseourl ident=$oViewConf->getSelfLink()|cat:"cl=account_order" }]" rel="nofollow">
							[{ oxmultilang ident="ORDER_HISTORY" }]
						</a><br>
						[{ oxmultilang ident="ORDERS" suffix="COLON" }] [{ $oView->getOrderCnt() }]
					</li>
					<li class="list-group-item">
						<a id="linkMyShoppingLists"
						   href="[{ oxgetseourl ident=$oViewConf->getSelfLink()|cat:"cl=myfavorites&fnc=showLists" }]"
						   rel="nofollow">
							[{ oxmultilang ident="BTN_MYFAVORITES_LISTS" }]
						</a><br>
						[{ oxmultilang ident="MYFAVORITES_LISTS_DESC" }]
					</li>
				[{/block}]
		</div>
		<div class="panel-footer" style="overflow: hidden;">
			<a href="[{$oViewConf->getLogoutLink() }]" style="margin-bottom:0;margin-right:0;float:right;" type="button"
			   class="btn btn-default ladda-button btn-danger">
				<span class="glyphicon glyphicon-off"></span>&nbsp;[{oxmultilang ident="LOGOUT"}]</a>
		</div>
	</div>
	[{insert name="oxid_tracker" title=$template_title }]
[{/capture}]
[{capture append="oxidBlock_sidebar"}]
	[{include file="page/account/inc/account_menu.tpl"}]
[{/capture}]
[{*include file="layout/page.tpl" sidebar="Left"*}]
[{include file="layout/page.tpl" sidebarLeft=1 sidebarRight=1}]
