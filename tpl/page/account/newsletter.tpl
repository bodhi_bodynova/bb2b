[{capture append="oxidBlock_content"}]
	[{if $oView->getSubscriptionStatus() != 0 }]
		[{if $oView->getSubscriptionStatus() == 1 }]
			<div class="status success corners">[{oxmultilang ident="MESSAGE_NEWSLETTER_SUBSCRIPTION_SUCCESS"}]</div>
		[{else }]
			<div class="status success corners">[{oxmultilang ident="MESSAGE_NEWSLETTER_SUBSCRIPTION_CANCELED"}]</div>
		[{/if}]
	[{/if}]
	<div class="panel panel-default">
		<div class="panel-heading">
			<h1 class="panel-title">[{$oView->getTitle()}]
				"</h1>
			<div class="pull-right" style="margin-top: -27px;">
				[{if $oView->getClassName() eq "account_newsletter" && !$blHideBreadcrumb}]
					[{include file="widget/breadcrumb.tpl"}]
				[{/if}]
			</div>
		</div>
		<div class="panel-body">
			[{include file="form/account_newsletter.tpl"}]
		</div>
	</div>
[{/capture}]


[{*capture append="oxidBlock_sidebar"}]
    [{include file="page/account/inc/account_menu.tpl" active_link="newsletter"}]
[{/capture*}]

[{include file="layout/page.tpl" sidebarLeft=1 sidebarRight=1}]
