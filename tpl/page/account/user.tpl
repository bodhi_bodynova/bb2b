[{capture append="oxidBlock_content"}]
    [{assign var="template_title" value="BILLING_SHIPPING_SETTINGS"|oxmultilangassign }]
    <div class="panel panel-default">
        <div class="panel-heading">
            <h1 class="panel-title">[{ $template_title }]</h1>
            <div class="pull-right" style="margin-top: -27px;">
                [{if $oView->getClassName() eq "account_user" && !$blHideBreadcrumb}]
                    [{include file="widget/breadcrumb.tpl"}]
                [{/if}]
            </div>
        </div>
        <div class="panel-body">
            [{block name="account_user_form"}]
                [{include file="form/user.tpl"}]
            [{/block}]
            [{ insert name="oxid_tracker" title=$template_title }]
        </div>
	</div>
[{/capture}]

[{capture append="oxidBlock_sidebar"}]
	[{include file="page/account/inc/account_menu.tpl" active_link="billship"}]
[{/capture}]

[{include file="layout/page.tpl" sidebarLeft=1 sidebarRight=1 sidebar="Left"}]
