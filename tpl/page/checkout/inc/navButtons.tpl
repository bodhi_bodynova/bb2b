<nav>
	<ul class="pager">
		<li class="previous"><a href="#"><span aria-hidden="true">&larr;</span>[{oxmultilang ident="PREVIOUS_STEP"}]</a></li>
		<li class="next"><a href="#">[{oxmultilang ident="SUBMIT_ORDER"}]<span aria-hidden="true">&rarr;</span></a></li>
	</ul>
</nav>

<a href="[{oxgetseourl ident=$oViewConf->getOrderLink()}]" class="prevStep submitButton largeButton btn btn-warning">[{oxmultilang ident="PREVIOUS_STEP"}]</a>
<button type="submit" class="submitButton nextStep largeButton btn btn-success pull-right">[{oxmultilang ident="SUBMIT_ORDER"}]</button>
