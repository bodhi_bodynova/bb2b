[{capture append="oxidBlock_content"}]
<div class="panel panel-default">
	<div class="panel-heading">
		<h1 class="panel-title">[{$oView->getTitle()}]</h1>
		<div class="pull-right" style="margin-top: -27px;">
			[{if $oView->getClassName() eq "order" && !$blHideBreadcrumb}]
				[{include file="widget/breadcrumb.tpl"}]
			[{/if}]
		</div>
	</div>
	<div class="panel-body" style="padding:15px">

	[{* Fehlermeldungen: *}]
    [{block name="checkout_order_errors"}]
        [{if $oView->isConfirmAGBError() == 1}]
            [{include file="message/error.tpl" statusMessage="READ_AND_CONFIRM_TERMS"|oxmultilangassign}]
        [{/if}]
        [{assign var="iError" value=$oView->getAddressError()}]
        [{if $iError == 1}]
           [{include file="message/error.tpl" statusMessage="ERROR_DELIVERY_ADDRESS_WAS_CHANGED_DURING_CHECKOUT"|oxmultilangassign}]
        [{/if}]
    [{/block}]

    [{* Checkout Steps *}]
    [{include file="page/checkout/inc/steps.tpl" active=4}]


	[{block name="checkout_order_main"}]

		[{* Message to Check *}]
		[{if !$oView->showOrderButtonOnTop()}]
            <div class="alert alert-warning" role="alert">
                [{oxmultilang ident="MESSAGE_SUBMIT_BOTTOM"}]
            </div>
        [{/if}]

        [{block name="checkout_order_details"}]
            [{if !$oxcmp_basket->getProductsCount()}]
                [{block name="checkout_order_emptyshippingcart"}]
                    <div class="alert alert-danger" role="alert">[{oxmultilang ident="BASKET_EMPTY"}]</div>
                [{/block}]
            [{else}]
                [{assign var="currency" value=$oView->getActCurrency()}]

                [{if $oView->isLowOrderPrice()}]
                    [{block name="checkout_order_loworderprice_top"}]
                        <div class="alert alert-warning" role="alert">[{oxmultilang ident="MIN_ORDER_PRICE"}] [{$oView->getMinOrderPrice()}] [{$currency->sign}]</div>
                    [{/block}]
                [{else}]
                    [{*if $oView->showOrderButtonOnTop()}]
                        <div id="orderAgbTop">
                            <form role="form" action="[{$oViewConf->getSslSelfLink()}]" method="post" id="orderConfirmAgbTop">
                                <div class="form-group">
                                    [{$oViewConf->getHiddenSid()}]
                                    [{$oViewConf->getNavFormParams()}]
                                    <input type="hidden" name="cl" value="order">
                                    <input type="hidden" name="fnc" value="[{$oView->getExecuteFnc()}]">
                                    <input type="hidden" name="challenge" value="[{$challenge}]">
                                    <input type="hidden" name="sDeliveryAddressMD5" value="[{$oView->getDeliveryAddressMD5()}]">

                                    [{include file="page/checkout/inc/agb.tpl"}]

                                    <div class="lineBox clear">
                                        <a href="[{oxgetseourl ident=$oViewConf->getPaymentLink()}]" class="prevStep submitButton largeButton">[{oxmultilang ident="PREVIOUS_STEP"}]</a>
                                        <button type="submit" class="submitButton nextStep largeButton btn btn-success">[{oxmultilang ident="SUBMIT_ORDER"}]</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    [{/if*}]
                [{/if}]

				[{*$oxcmp_basket->getIsWhiteLabel()|var_dump*}]
				[{*$oxcmp_basket|var_dump*}]
                [{block name="checkout_order_vouchers"}]
                    [{if $oViewConf->getShowVouchers() && $oxcmp_basket->getVouchers()}]
                        [{oxmultilang ident="USED_COUPONS"}]
                        <div>
                            [{foreach from=$Errors.basket item=oEr key=key}]
                                [{if $oEr->getErrorClassType() == 'oxVoucherException'}]
                                    [{oxmultilang ident="COUPON_NOT_ACCEPTED" args=$oEr->getValue('voucherNr')}]<br>
                                    [{oxmultilang ident="REASON" suffix="COLON"}]
                                    [{$oEr->getOxMessage()}]<br>
                                [{/if}]
                            [{/foreach}]
                            [{foreach from=$oxcmp_basket->getVouchers() item=sVoucher key=key name=aVouchers}]
                                [{$sVoucher->sVoucherNr}]<br>
                            [{/foreach}]
                        </div>
                    [{/if}]
                [{/block}]

                [{block name="checkout_order_address"}]
                    <div id="orderAddress">
                        <form role="form form-horizontal" action="[{$oViewConf->getSslSelfLink()}]" method="post">
                            <div class="form-group">
	                            <label class="col-sm-2 control-label norm"><strong>[{oxmultilang ident="ADDRESSES"}]</strong></label>
	                            <div class="col-sm-10">
		                            [{$oViewConf->getHiddenSid()}]
		                            <input type="hidden" name="cl" value="user">
		                            <input type="hidden" name="fnc" value="">
		                            <button type="submit" class="submitButton btn btn-info">[{oxmultilang ident="EDIT"}]&nbsp;<i class="glyphicon glyphicon-pencil"></i></button>
	                            </div>
                            </div>
                        </form>
	                    [{assign var="oDelAdress" value=$oView->getDelAddress()}]
	                    <div class="container-fluid">
		                    <div class="row">
		                        <!--<div class="col-sm-2">[{oxmultilang ident="BILLING_ADDRESS" suffix="COLON"}]</div>-->
			                    <div class="col-sm-5">
				                    <h5>[{oxmultilang ident="BILLING_ADDRESS"}]</h5>
				                    [{include file="widget/address/billing_address.tpl"}]
			                    </div>


			                    <!--<div class="col-sm-2">[{oxmultilang ident="SHIPPING_ADDRESS" suffix="COLON"}]</div>-->
			                    [{if $oDelAdress}]
				                    <div class="col-sm-5">
					                    <h5>[{oxmultilang ident="SHIPPING_ADDRESS"}]</h5>
					                    [{include file="widget/address/shipping_address.tpl" delivadr=$oDelAdress}]
					                </div>
			                    [{/if}]
			                </div>
		                    [{if $oView->getOrderRemark()}]
			                    <div class="row">
				                    <div class="col-xs-12">
					                    <h4>[{oxmultilang ident="WHAT_I_WANTED_TO_SAY" suffix="COLON"}]</h4>
					                    <div>
						                    [{$oView->getOrderRemark()}]
					                    </div>
				                    </div>
			                    </div>
		                    [{/if}]
	                    </div>


                        [{*assign var="oDelAdress" value=$oView->getDelAddress()}]
                        [{if $oDelAdress}]
                            <dl class="shippingAddress">
                                <dt>[{oxmultilang ident="SHIPPING_ADDRESS"}]</dt>
                                <dd>
                                    [{include file="widget/address/shipping_address.tpl" delivadr=$oDelAdress}]
                                </dd>
                            </dl>
                        [{/if}]
                        [{if $oView->getOrderRemark()}]
                            <dl class="orderRemarks">
                                <dt>[{oxmultilang ident="WHAT_I_WANTED_TO_SAY"}]</dt>
                                <dd>
                                    [{$oView->getOrderRemark()}]
                                </dd>
                            </dl>
                        [{/if*}]
                    </div>
                    <div style="clear:both;"></div>
                [{/block}]

                [{*block name="shippingAndPayment"}]
                    <div id="orderShipping">
                    <form role="form" action="[{$oViewConf->getSslSelfLink()}]" method="post">
                        <div class="form-group">
                            <h3 class="section">
                                <strong>[{oxmultilang ident="SHIPPING_CARRIER"}]</strong>
                                [{$oViewConf->getHiddenSid()}]
                                <input type="hidden" name="cl" value="payment">
                                <input type="hidden" name="fnc" value="">
                                <button type="submit" class="submitButton largeButton btn btn-info">[{oxmultilang ident="EDIT"}]</button>
                            </h3>
                        </div>
                    </form>
                    [{assign var="oShipSet" value=$oView->getShipSet()}]
                    [{$oShipSet->oxdeliveryset__oxtitle->value}]
                    </div>
                    <div id="orderPayment">
                        <form role="form" action="[{$oViewConf->getSslSelfLink()}]" method="post">
                            <div class="form-group">
                                <h3 class="section">
                                    <strong>[{oxmultilang ident="PAYMENT_METHOD"}]</strong>
                                    [{$oViewConf->getHiddenSid()}]
                                    <input type="hidden" name="cl" value="payment">
                                    <input type="hidden" name="fnc" value="">
                                    <button type="submit" class="submitButton largeButton btn btn-info">[{oxmultilang ident="EDIT"}]</button>
                                </h3>
                            </div>
                        </form>
                        [{assign var="payment" value=$oView->getPayment()}]
                        [{$payment->oxpayments__oxdesc->value}]
                    </div>
                [{/block*}]

                [{if $oView->isLowOrderPrice()}]
                    [{block name="checkout_order_loworderprice_bottom"}]
                        <div>[{oxmultilang ident="MIN_ORDER_PRICE"}] [{$oView->getMinOrderPrice()}] [{$currency->sign}]</div>
                    [{/block}]
                [{else}]
                    [{block name="checkout_order_btn_confirm_bottom"}]
                        <form role="form" action="[{$oViewConf->getSslSelfLink()}]" method="post" id="orderConfirmAgbBottom" enctype="multipart/form-data">
                            <div class="form-group">
                                [{$oViewConf->getHiddenSid()}]
                                [{$oViewConf->getNavFormParams()}]
                                <input type="hidden" name="cl" value="order">
                                <input type="hidden" name="fnc" value="[{$oView->getExecuteFnc()}]">
                                <input type="hidden" name="challenge" value="[{$challenge}]">
                                <input type="hidden" name="sDeliveryAddressMD5" value="[{$oView->getDeliveryAddressMD5()}]">
                                [{*if $oViewConf->isFunctionalityEnabled("blShowTSInternationalFeesMessage")}]
                                    [{oxifcontent ident="oxtsinternationalfees" object="oTSIFContent"}]
                                        <hr/>
                                        <div class="clear">
                                            <span class="title">[{$oTSIFContent->oxcontents__oxcontent->value}]</span>
                                        </div>
                                    [{/oxifcontent}]
                                [{/if*}]

								[{* Neu *}]
	                            <div class="form-group">
		                            <label style="margin-top: 10px;" class="col-sm-2 control-label norm" for="iswhitelabel">[{oxmultilang ident="NEUTRAL" suffix="COLON"}]</label>
		                            <div class="col-sm-10">
			                            <div class="alert alert-info" role="alert" style="margin-left: -15px">
				                            <input style="" class="" name="iswhitelabel" type="checkbox" id="iswhitelabel" value="[{if $oxcmp_basket->getIsWhiteLabel()}]1[{else}]0[{/if}]" [{if $oxcmp_basket->getIsWhiteLabel()}]checked[{/if}] > <!-- value="0" -->
				                            <span class="help-block">[{oxmultilang ident="NEUTRALER_VERSAND_HELP" }]</span>
				                            [{if $oxcmp_basket->getUpfile() == ''}]
					                            <div id="upload" style="display: none">
						                            <input  name="lieferschein" type="file" accept=".pdf"/>
						                            <span class="help-block">[{oxmultilang ident="QUICKORDER_PDF_TXT"}]</span>
					                            </div>
				                            [{else}]
					                            <div id="upload" style="display: block">
						                            <span class="help-block">Ihre Datei: (übersetzen) = [{$oxcmp_basket->getUpfile()}]</span>
					                            </div>
				                            [{/if}]

			                            </div>

		                            </div>
	                            </div>
	                            [{oxscript add="
/*console.log($('#iswhitelabel').val());
if($('#iswhitelabel').val() == 1){
	$('#iswhitelabel').prop('checked');
};*/
$('#iswhitelabel').change(function(){
							        if($('#iswhitelabel').prop('checked')){
							            $('#upload').css('display', 'block');
										$('#iswhitelabel').val(1);
							        }else{
							            $('#upload').css('display', 'none');
										$('#iswhitelabel').val(0);
							        }
							        /*console.log($('#iswhitelabel').prop('checked'));*/
							        });"}]

                                [{*Neutraler Versand*}]
                                    [{*}]<label class="col-sm-2 control-label">[{oxmultilang ident="QUICKORDER_WHITE_LABEL" suffix="COLON"}]</label>
                                    <div class="col-sm-10">
                                        <input name="iswhitelabel" type="checkbox">
                                    </div>
                                    <br><br>
                                    <label class="col-sm-2 control-label">[{oxmultilang ident="DATEI" suffix="COLON" }]</label>
                                    <div class="col-sm-10">
                                        [{oxmultilang ident="QUICKORDER_PDF_TXT"}]
                                            <input name="lieferschein" type="file" accept=".pdf"/>
                                    </div>
								[{*}]

                                [{*if $payment->oxpayments__oxid->value eq "oxidcashondel" && $oViewConf->isFunctionalityEnabled("blShowTSCODMessage")}]
                                    [{oxifcontent ident="oxtscodmessage" object="oTSCODContent"}]
                                        <hr/>
                                        <div class="clear">
                                            <span class="title">[{$oTSCODContent->oxcontents__oxcontent->value}]</span>
                                        </div>
                                    [{/oxifcontent}]
                                [{/if*}]

                                [{*if !$oView->showOrderButtonOnTop()}]
	                                [{* dies: *}]
                                    [{*include file="page/checkout/inc/agb.tpl"}]
	                                <hr/>
                                [{else}]
                                    [{include file="page/checkout/inc/agb.tpl" hideButtons=true}]
									<hr/>
                                [{/if*}]
	                            [{*debug*}]

	                            [{*}]<nav>
		                            <ul class="pager">
			                            <li class="previous"><a href="[{oxgetseourl ident=$oViewConf->getOrderLink()}]"><span aria-hidden="true">&larr;</span>[{oxmultilang ident="PREVIOUS_STEP"}]</a></li>
			                            <li class="next"><a href="#" onclick="$(this).closest('form').submit();" type="submit">[{oxmultilang ident="SUBMIT_ORDER"}]<span aria-hidden="true">&rarr;</span></a></li>
		                            </ul>
	                            </nav>[{*}]
	                            [{oxifcontent ident="oxrighttocancellegend2" object="oContent"}]
		                            <div class="form-group">
			                            <div class="col-sm-10 col-sm-offset-2">
				                            <span class="help-block">[{$oContent->oxcontents__oxcontent->value}]</span>
			                            </div>
		                            </div>
	                            [{/oxifcontent}]

                                <a href="[{oxgetseourl ident=$oViewConf->getOrderLink()}]" class="prevStep submitButton largeButton btn btn-warning"><i class="glyphicon glyphicon-arrow-left"></i>&nbsp;[{oxmultilang ident="PREVIOUS_STEP"}]</a>
                                <button type="submit" class="submitButton nextStep largeButton btn btn-success pull-right"><i class="glyphicon glyphicon-euro"></i>&nbsp;[{oxmultilang ident="SUBMIT_ORDER"}]</button>
                            </div>
                        </form>
                    [{/block}]
                [{/if}]
                <div id="orderEditCart">
                    <form role="form" action="[{$oViewConf->getSslSelfLink()}]" method="post">
                        <div class="form-group">
                            <h3 class="section">
                                <strong>[{oxmultilang ident="CART"}]</strong>
                                [{$oViewConf->getHiddenSid()}]
                                <input type="hidden" name="cl" value="basket">
                                <input type="hidden" name="fnc" value="">
                                <button type="submit" class="submitButton largeButton btn btn-info">[{oxmultilang ident="EDIT"}]&nbsp;<i class="glyphicon glyphicon-pencil"></i></button>
                            </h3>
                        </div>
                    </form>
                </div>
                <div class="lineBox clear">
                    [{block name="order_basket"}]
                        [{include file="page/checkout/inc/basketcontents.tpl" editable=false}]
                    [{/block}]
                </div>
            [{/if}]
        [{/block}]
    [{/block}]
    [{insert name="oxid_tracker" title=$template_title}]
</div>
</div>
[{/capture}]
[{assign var="template_title" value="REVIEW_YOUR_ORDER"|oxmultilangassign}]
[{include file="layout/page.tpl" title=$template_title location=$template_title sidebarLeft="1" sidebarRight="1"}]
