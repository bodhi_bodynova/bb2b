[{capture append="oxidBlock_content"}]
	<div class="panel panel-default">
		<div class="panel-heading">
			<h1 class="panel-title">[{*$oView->getTitle()*}][{$oxcmp_shop->oxshops__oxcompany->value}]</h1>
			<div class="pull-right" style="margin-top: -27px;">
				[{if $oView->getClassName() eq "contact" && !$blHideBreadcrumb}]
					[{include file="widget/breadcrumb.tpl"}]
				[{/if}]
			</div>
		</div>
		<div class="panel-body">
	    [{if $oView->getContactSendStatus() }]
	        [{assign var="_statusMessage" value="THANK_YOU_MESSAGE"|oxmultilangassign:$oxcmp_shop->oxshops__oxname->value}]
	        [{include file="message/notice.tpl" statusMessage=$_statusMessage}]
	    [{/if }]
	    [{*}]<h1 class="pageHead">[{ $oxcmp_shop->oxshops__oxcompany->value }]</h1>[{*}]

			<div class="row">
				<div class="container-fluid">
					[{include file="form/contact.tpl"}]
				</div>
			</div>

	    [{ insert name="oxid_tracker" title=$template_title }]
		</div>
	</div>
[{/capture}]

[{include file="layout/page.tpl" sidebarLeft=1 sidebarRight=1}]
[{*include file="layout/page.tpl" sidebar="Left"*}]
