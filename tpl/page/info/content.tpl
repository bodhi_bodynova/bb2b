[{capture append="oxidBlock_content"}]
    [{assign var="oContent" value=$oView->getContent()}]
    [{assign var="tpl" value=$oViewConf->getActTplName()}]
    [{assign var="oxloadid" value=$oViewConf->getActContentLoadId()}]

	<div class="panel panel-default">
	    <div class="panel-heading">
	        <h3 class="panel-title">[{$oView->getTitle()}]</h3>
	        <div class="pull-right" style="margin-top: -27px;">
	            [{if $oView->getClassName() eq "content" && !$blHideBreadcrumb}]
	                [{include file="widget/breadcrumb.tpl"}]
	            [{/if}]
	        </div>
	    </div>
	    <div class="panel-body">
	        [{$oView->getParsedContent()}]
	    </div>
	</div>
[{/capture}]
[{include file="layout/page.tpl" sidebar="Left" sidebarLeft=1 sidebarRight=1}]
