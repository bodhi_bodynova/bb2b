[{assign var="aServices" value=$oView->getServicesList()}]
[{assign var="aServiceItems" value=$oView->getServicesKeys()}]
[{block name="footer_information"}]
<div class="panel panel-default">
	<div class="panel-heading">
		<h3 class="panel-title">[{oxmultilang ident="INFORMATION" }]</h3>
	</div>
	<div class="panel-body">
		<ul class="tree nav nav-pills nav-stacked" role="tablist">
			[{foreach from=$aServiceItems item=sItem}]
				[{if isset($aServices.$sItem)}]
					<li><a href="[{$aServices.$sItem->getLink()}]">[{$aServices.$sItem->oxcontents__oxtitle->value}]</a></li>
				[{/if}]
			[{/foreach}]
			[{oxifcontent ident="works" object="oCont"}]
				<li><a href="[{$oCont->getLink()}]">[{$oCont->oxcontents__oxtitle->value}]</a></li>
			[{/oxifcontent}]
		</ul>
	</div>
	<div class="panel-footer">
		<span class="headline"><strong>&copy; 2019 Bodynova GmbH</strong></span>
	</div>
</div>
[{/block}]
