[{block name="footer_services"}]
	[{assign var="dropship" value=$oxcmp_user->oxuser__dropship->value}]
	<div class="panel panel-default">
		<div class="panel-heading">
			<h3 class="panel-title">[{oxmultilang ident="SERVICES" }]</h3>
		</div>
		<div class="panel-body">
			<ul class="tree nav nav-pills nav-stacked" role="tablist">
				<li ><a href="[{oxgetseourl ident=$oViewConf->getSelfLink()|cat:"cl=contact" }]">[{ oxmultilang ident="CONTACT" }]</a></li>
				<li ><a href="[{oxgetseourl ident=$oViewConf->getSelfLink()|cat:"cl=account" }]" rel="nofollow">[{ oxmultilang ident="ACCOUNT" }]</a></li>
				<li ><a href="[{oxgetseourl ident=$oViewConf->getSelfLink()|cat:"cl=myfavorites&fnc=showLists" }]" rel="nofollow">[{ oxmultilang ident="BTN_MYFAVORITES_LISTS" }]</a></li>
				[{*if $dropship == 1}]
					<li ><a href="[{oxgetseourl ident=$oViewConf->getSelfLink()|cat:"cl=quickorder&fnc=showForm" }]" rel="nofollow">[{ oxmultilang ident="BTN_QUICKORDER" }]</a></li>
				[{/if*}]
			</ul>
		</div>
		<div class="panel-footer" style="overflow: hidden;">
			<a href="[{$oViewConf->getLogoutLink() }]" type="button" class="btn btn-xs btn-danger pull-right bntooltip" data-placement="top" title="[{oxmultilang ident="LOGOUT"}]"><span class="glyphicon glyphicon-off"></span></a>
		</div>
	</div>
[{/block}]
