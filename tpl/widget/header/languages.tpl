[{if $oView->isLanguageLoaded()}]
    <!-- Flaggen-Box -->
    <div id="languageSwitcher" class="dropdown symbol-flags" >
        <button id="btnLangaugeMenue" class="btn btn-default dropdown-toggle" aria-expanded="true" data-toggle="dropdown" type="button">
            [{foreach from=$oxcmp_lang item=_lng}]
                [{if $_lng->selected}]
                    [{assign var="sLangImg" value=""|cat:$_lng->abbr|cat:".png"}]
                    <img src="[{$oViewConf->getImageUrl($sLangImg)}]">
                    [{*$_lng->name*}]
                [{/if}]
            [{/foreach}]
            <span class="caret"></span>
        </button>
        <ul class="dropdown-menu" aria-labelledby="btnLangaugeMenue" role="menu" style="top:0 !important; left: auto !important; min-width: 1px;">
            [{foreach from=$oxcmp_lang item=_lng}]
                [{assign var="sLangImg" value="lang/"|cat:$_lng->abbr|cat:".png"}]
                <li>
                    <a class="flag [{$_lng->abbr }] [{if $_lng->selected}]selected[{/if}]" title="[{$_lng->name}]" href="[{$_lng->link|oxaddparams:$oView->getDynUrlParams()}]" hreflang="[{$_lng->abbr }]">
                        <img src="[{$oViewConf->getImageUrl($sLangImg)}]">
                        [{*$_lng->name*}]
                    </a>
                </li>
            [{/foreach}]
        </ul>
    </div><!-- Ende: Flaggen-Box -->

    <!-- Logout Symbol -->
    <!--<div class="symbol-logout col-xs-3 navbar-right">-->
        <a class="btn btn-default btn-sm btn-danger" title="[{oxmultilang ident="LOGOUT"}]" data-placement="bottom" data-toggle="tooltip" href="[{$oViewConf->getLogoutLink()}]">
            <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
        </a>
    <!--</div>--><!-- Ende: Logout Symbol -->
    </div>
    </form>
    </li>
[{/if}]
[{oxscript widget=$oView->getClassName()}]
