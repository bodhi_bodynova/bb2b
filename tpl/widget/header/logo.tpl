<!-- Logo -->
<li role="presentation" class="logo hidden-xs hidden-sm">
    <a href="[{$oViewConf->getHomeLink()}]" title="[{$oxcmp_shop->oxshops__oxtitleprefix->value}]">
        <img alt="Bodynova Logo" src="[{$oViewConf->getImageUrl('logo-small.png')}]">
    </a>
</li>
