<noscript>
	<div id="nojava">
		Um unseren Shop korrekt nutzen Sie können, aktivieren Sie bitte JavaScript in Ihren Browsereinstellungen.</div>
	<br />
</noscript>

<!-- Navbar oben fixiert: -->
<nav class="navbar navbar-default navbar-fixed-top nav nav-tabs nav-justified" id="header">

	<!-- Navigation -->
	<ul class="nav nav-tabs nav-justified">

		<div class="row container-fluid">

			[{* Logo: *}]
			<div class="hidden-xs hidden-sm col-md-3 col-lg-3">
				<div class="center-block" style="text-align: center">
					[{include file="widget/header/logo.tpl"}]
				</div>
			</div>
			<div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">

				<div class="row">
					[{* Suche *}]
					<div class="col-xs-12 col-sm-6 col-md-7 col-lg-7">
						[{if $oViewConf->getUser()}]
							[{include file="widget/header/search.tpl"}]
						[{/if}]
					</div>

					[{* Servicebox *}]
					<div class="col-xs-12 col-sm-6 col-md-5 col-lg-5">
						<div class="center-block" style="text-align: center">
							[{if $oViewConf->getUser()}]
								[{oxid_include_widget cl="oxwMiniBasket" nocookie=$blAnon force_sid=$force_sid}]
							[{/if}]
						</div>
					</div>
				</div>
			</div>

		</div>

		<div class="row container-fluid">
			<!--<div class="col-xs-12">-->
				[{* Navi-Leiste *}]
				[{if $oViewConf->getUser()}]
					[{include file="widget/header/navi.tpl"}]
				[{/if}]
			<!--</div>-->
		</div>

	</ul><!-- Ende: Navigation -->

</nav><!-- Ende: Navbar oben fixiert: -->
