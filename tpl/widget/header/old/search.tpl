[{* wird ersetzt durch: modules/autosuggest/views/blocks/search.tpl *}]
[{block name="widget_header_search_form"}]
    [{if $oView->showSearch() }]
        <div id="searchBox" class="pull-left">
            <form class="search" action="[{$oViewConf->getSelfActionLink() }]" method="get" name="search">
                <div class="searchBox">
                    [{ $oViewConf->getHiddenSid() }]
                    <input type="hidden" name="cl" value="search">
                    [{block name="header_search_field"}]
                        <input class="textbox" type="text" id="searchParam" name="searchparam" placeholder="[{oxmultilang ident="SEARCH" }]" value="[{$oView->getSearchParamForHtml()}]">
                    [{/block}]
                    <input class="searchSubmit" type="submit" value="">
                </div>
            </form>
        </div>
    [{/if}]
[{/block}]
