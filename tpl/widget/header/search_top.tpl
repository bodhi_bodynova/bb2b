[{block name="widget_header_search_form_top"}]
    [{if $oView->showSearch()}]
        <form class="form search" role="form" action="[{$oViewConf->getSelfActionLink()}]" method="get" name="search">
            [{$oViewConf->getHiddenSid()}]
            <input type="hidden" name="cl" value="search">

            [{block name="dd_widget_header_search_form_inner"}]
                <div class="input-group">
                    [{block name="header_search_field"}]
                        <input class="form-control" type="text" id="searchParamBottom" name="searchparam" value="[{$oView->getSearchParamForHtml()}]" placeholder="[{oxmultilang ident="SEARCH"}]">
                    [{/block}]

                    [{block name="dd_header_search_top"}]
                        <span class="input-group-btn">
                            <button type="submit" id="suchbutton" class="btn btn-primary" title="[{oxmultilang ident="SEARCH_SUBMIT"}]">
                                <span class="icon-search_stil fa-2x"></span>
                            </button>
                        </span>
                    [{/block}]
                </div>
            [{/block}]
        </form>
    [{/if}]
[{/block}]
