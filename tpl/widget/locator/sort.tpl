[{if $oView->showSorting()}]
    [{assign var="_listType" value=$oView->getListDisplayType()}]
    [{assign var="_additionalParams" value=$oView->getAdditionalParams()}]
    [{assign var="_artPerPage" value=$oViewConf->getArtPerPageCount()}]
    [{assign var="_sortColumnVarName" value=$oView->getSortOrderByParameterName()}]
    [{assign var="_sortDirectionVarName" value=$oView->getSortOrderParameterName()}]

	<div id="itemsPerPage" class="dropdown">
		<button type="button" class="btn btn-default" data-toggle="dropdown">
			[{ oxmultilang ident="SORT_BY" suffix="COLON" }]
			[{if $oView->getListOrderBy() }]
				[{oxmultilang ident=$oView->getListOrderBy()|upper }]
			[{else}]
				[{oxmultilang ident="CHOOSE"}]
			[{/if}]
		</button>
		<ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">
			[{foreach from=$oView->getSortColumns() item=sColumnName}]
				<li class="desc">
					<a href="[{ $oView->getLink()|oxaddparams:"ldtype=$_listType&amp;_artperpage=$_artPerPage&amp;$_sortColumnVarName=$sColumnName&amp;$_sortDirectionVarName=desc&amp;pgNr=0&amp;$_additionalParams"}]" [{if $oView->getListOrderDirection() == 'desc' && $sColumnName == $oView->getListOrderBy()}] class="selected"[{/if}]>
						<span>[{ oxmultilang ident=$sColumnName|upper }]</span>
						<span class="fa fa-sort-desc "></span>
					</a>
				</li>
				<li class="asc">
					<a href="[{ $oView->getLink()|oxaddparams:"ldtype=$_listType&amp;_artperpage=$_artPerPage&amp;$_sortColumnVarName=$sColumnName&amp;$_sortDirectionVarName=asc&amp;pgNr=0&amp;$_additionalParams"}]" [{if $oView->getListOrderDirection() == 'asc' && $sColumnName == $oView->getListOrderBy()}] class="selected"[{/if}]>
						<span>[{ oxmultilang ident=$sColumnName|upper }]</span>
						<span class="fa fa-sort-asc "></span>
					</a>
				</li>
			[{/foreach}]
		</ul>
	</div>
[{/if}]