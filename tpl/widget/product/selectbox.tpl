[{assign var="oSelections" value=$oSelectionList->getSelections()}]
[{if $oSelections}]
	<select id="" name="[{$sFieldName|default:"varselid"}][[{$iKey}]]">
		<option>
			[{$oSelectionList->getLabel()}]:
			[{assign var="oActiveSelection" value=$oSelectionList->getActiveSelection()}]
			[{if $oActiveSelection}]
				[{$oActiveSelection->getName()}]
			[{elseif !$blHideDefault}]
				[{if $sFieldName == "sel" }]
					[{ oxmultilang ident="PLEASE_CHOOSE" }]
				[{else}]
					[{ oxmultilang ident="CHOOSE_VARIANT" }]
				[{/if}]
			[{/if}]
		</option>
		[{foreach from=$oSelections item=oSelection}]
			<option value="[{$oSelection->getValue()}]">[{$oSelection->getName()}]</option>
		[{/foreach}]
	</select>
	<!--  value="[{if $oActiveSelection }][{$oActiveSelection->getValue()}][{/if}]" -->
[{else}]
	<a href="[{ $_productLink }]" class="variantMessage">
		[{if $sFieldName == "sel" }]
			[{ oxmultilang ident="PLEASE_CHOOSE" }]
		[{else}]
			[{ oxmultilang ident="CHOOSE_VARIANT" }]
		[{/if}]
	</a>
[{/if}]