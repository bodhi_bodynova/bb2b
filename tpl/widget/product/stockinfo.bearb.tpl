[{assign var="status" value=""}]
[{*}]<h4 class="debug">Status = [{$status}]</h4>[{*}]
[{if !$product || $product->isBuyable()}]
    <h4 class="debug">Status = [{$status}]</h4>
    [{* $prodct oder kaufbar (standartartikel) *}]

    [{assign var="BnFlag" value=$product->getBnFlag()}]
    [{if $BnFlag}]
        [{assign var="BnFlagStatus" value=$product->getBnFlagStatus()}]
        [{assign var="status" value='$product und kaufbar'}]
        <div class="debug">
            Bnflag = [{$BnFlag}]
            <div class="stockFlagBox [{if $BnFlag}][{$BnFlagStatus}][{/if}]"></div>
        </div>
    [{else}]
        <hr>
        Bnflag = gibts nicht
        <hr>
    [{/if}]

    [{*}]
        <p class="debug">
            !$product || $product->isBuyable() == true
            <pre>
                [{php}]
                    print_r($this->get_template_vars('BnFlag'));
                    #exit;
                [{/php}]
            </pre>
        </p>
    [{*}]

    [{if $product->oxarticles__bnflagbestand->value == 2}]
        [{*}]<!-- Der Artikel ist nicht auf Lager und muss erst nachbestellt werden -->[{*}]
        <div class="stockFlagBox notOnStock"></div>
        <span class="stockFlag notOnStock">
			[{if $product->oxarticles__oxnostocktext->value}]
                [{$product->oxarticles__oxnostocktext->value}]
            [{elseif $oViewConf->getStockOffDefaultMessage()}]
                [{oxmultilang ident="MESSAGE_NOT_ON_STOCK"}]
            [{/if}]
            [{if $product->getDeliveryDate()}]
                [{oxmultilang ident="AVAILABLE_ON"}] [{$product->getDeliveryDate()}]
            [{/if}]
		</span>
        [{*}]<!--[{elseif $oDetailsProduct->getStockStatus() == 1}]-->[{*}]
    [{elseif $product->oxarticles__bnflagbestand->value == 1}]


        [{*}]<!-- Wenige Exemplare auf Lager -schnell bestellen! -->[{*}]
        <div class="stockFlagBox lowStock"></div>
        <span class="stockFlag lowStock">
			[{oxmultilang ident="LOW_STOCK"}]
		</span>
        [{*}]<!--[{elseif $oDetailsProduct->getStockStatus() == 0}]-->[{*}]
        [{*elseif $_product->oxarticles__bnflagbestand->value == 0*}]
    [{/if}]

    [{if $product}]
        [{*}]<!-- Standartarticle -->[{*}]
        [{if $_product}]
            [{*debug*}]
            [{*$product | var_dump*}]

            [{* Ampel grün! *}]
            [{if $_product->oxarticles__bnflagbestand->value == 0}]
                <div class="stockFlagBox onStock"></div>
                <span class="stockFlag">
                    [{if $_product->oxarticles__oxstocktext->value}]
                        [{$_product->oxarticles__oxstocktext->value}]
                    [{elseif $oViewConf->getStockOnDefaultMessage()}]
                        [{oxmultilang ident="READY_FOR_SHIPPING"}]
                    [{/if}]
                </span>
            [{/if}]


            [{* Ampel gelb *}]
            [{if $_product->oxarticles__bnflagbestand->value == 1}]
                <div class="stockFlagBox lowStock"></div>
                <span class="stockFlag lowStock">
                [{oxmultilang ident="LOW_STOCK"}]
                </span>
            [{/if}]


            [{* Ampel rot *}]
            [{if $_product->oxarticles__bnflagbestand->value == 2}]
                <div class="stockFlagBox notOnStock"></div>
                <span class="stockFlag notOnStock">
                    [{if $_product->oxarticles__oxnostocktext->value}]
                        [{$_product->oxarticles__oxnostocktext->value}]
                    [{elseif $oViewConf->getStockOffDefaultMessage()}]
                        [{oxmultilang ident="MESSAGE_NOT_ON_STOCK"}]
                    [{/if}]
                    [{if $_product->getDeliveryDate()}]
                        [{oxmultilang ident="AVAILABLE_ON"}] [{$_product->getDeliveryDate()}]
                    [{/if}]
		        </span>
            [{/if}]
        [{/if}]



        [{*/if*}]
        [{if !$product}]
            [{if $_product.mindeltime==0 || $_product.maxdeltime==0}]
                [{*$_product|var_dump*}]
                [{*if $_product->getParentLieferzeit()}]
                    ddd
                    <div class="stockFlagBox onStock"></div>
                    [{oxmultilang ident="DELIVERYTIME_DELIVERYTIME" suffix="COLON"}]
                    [{assign var="ident" value=$product->getParentLieferzeit()}]
                    [{oxmultilang ident=$ident args=$product->getParentLieferzeit()}]
                    [{oxmultilang ident="DELTIMEUNIT"}]
                [{/if*}]

                a: <div class="stockFlagBox onStock"></div>
                <span class="stockFlag">
                    [{if $product->oxarticles__oxstocktext->value}]
                        [{$product->oxarticles__oxstocktext->value}]
                    [{elseif $oViewConf->getStockOnDefaultMessage()}]
                        [{oxmultilang ident="READY_FOR_SHIPPING"}]
                    [{/if}]
                </span>
            [{/if}]
        [{/if}]
    [{/if}]
[{else}]
    [{assign var="status" value='!$product oder Artikel nicht kaufbar'}]
    <h4 class="debug">Status = [{$status}]</h4>

    [{*}]
        <span class="debug">
            !$product || $product->isBuyable() == true<br/>
            <pre>
        [{php}]
            print_r($this->get_template_vars('product'));
            #exit;
        [{/php}]
        </pre>

        </span>
    [{*}]
    [{if !$product->getVariantsCount() > 0}]

         [{*!$product->getVariantsCount() > 0*}]

        [{if $product->oxarticles__oxmindeltime->value || $product->oxarticle__oxmaxdeltime->value}]
            [{oxmultilang ident="DELIVERYTIME_DELIVERYTIME" suffix="COLON"}]
            [{if $product->oxarticles__oxmindeltime->value != $product->oxarticle__oxmaxdeltime->value}]
                [{$product->oxarticle__oxmaxdeltime->value}] -
            [{/if}]
            [{if $product->oxarticle__oxmaxdeltime->value}]
                [{assign var="unit" value=$_product.delunit}]
                [{assign var="ident" value=DELIVERYTIME_$unit}]
                [{if $product->oxarticle__oxmaxdeltime->value > 1}]
                    [{assign var="ident" value=$ident|cat:"S"}]
                [{/if}]
                [{oxmultilang ident=$ident args=$product->oxarticle__oxmaxdeltime->value}]
            [{/if}]
        [{/if}]
    [{/if}]
    [{if !$product->isBuyable() && !$product->getVariantsCount() > 0 }]
        [{if $product->oxarticles__oxmindeltime->value || $product->oxarticle__oxmaxdeltime->value}]
            [{oxmultilang ident="DELIVERYTIME_DELIVERYTIME" suffix="COLON"}]
            [{if $product->oxarticles__oxmindeltime->value != $product->oxarticle__oxmaxdeltime->value}]
                [{$product->oxarticle__oxmaxdeltime->value}] -
            [{/if}]
            [{if $product->oxarticle__oxmaxdeltime->value}]
                [{assign var="unit" value=$_product.delunit}]
                [{assign var="ident" value=DELIVERYTIME_$unit}]
                [{if $product->oxarticle__oxmaxdeltime->value > 1}]
                    [{assign var="ident" value=$ident|cat:"S"}]
                [{/if}]
                [{oxmultilang ident=$ident args=$product->oxarticle__oxmaxdeltime->value}]
            [{/if}]
        [{/if}]
    [{/if}]
    [{* Lieferzeit bei der Varianten bei den Vätern anzeigen: *}]
    [{if $_product}]
        [{if $product->getParentLieferzeit()}]
            <div class="stockFlagBox onStock">

            </div>
            [{oxmultilang ident="DELIVERYTIME_DELIVERYTIME" suffix="COLON"}]
            [{assign var="ident" value=$product->getParentLieferzeit()}]
            [{oxmultilang ident=$ident args=$product->getParentLieferzeit()}]
            [{oxmultilang ident="DELTIMEUNIT"}]
        [{/if}]
    [{/if}]
[{/if}]
